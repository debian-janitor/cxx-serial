libcxx-serial (1.2.1-5) unstable; urgency=medium

  * Handle uninstalled cmake configuration files. Closes: #997732

 -- Alec Leamas <leamas.alec@gmail.com>  Sun, 24 Oct 2021 15:01:30 +0200

libcxx-serial (1.2.1-4.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Adopt to catkin installing pkgconfig to share (Closes: #977844)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 21 Dec 2020 20:38:16 +0100

libcxx-serial (1.2.1-4) unstable; urgency=medium

  * Add missing build-deps libgmock-dev and pkg-config. Closes: #973112

 -- Alec Leamas <leamas.alec@gmail.com>  Wed, 28 Oct 2020 18:33:57 +0100

libcxx-serial (1.2.1-3) unstable; urgency=medium

  * Fix FTBFS on i386. Kudos: Gianfranco Costamagna

 -- Alec Leamas <leamas.alec@gmail.com>  Wed, 23 Sep 2020 11:20:51 +0200

libcxx-serial (1.2.1-2) unstable; urgency=medium

  * Drop v8stdint.h header (not used in Debian). Closes: #921697
  * Update python-catkin-pkg BR to python3.
  * Standards-version -> 4.5.0, no changes.
  * Update dephelper-compat -> 12
  * Set Rules-Requires-Root
  * Add d/upstream/metadata
  * Handle DEB_BUILD_OPTIONS when overriding test target.
  * Clean up test target in d/rules,
  * Simplify d/rules using --buildsystem=cmake.
  * Reorganize repo branches according to dep-14.
  * Fix lingering templates in d/watch.
  * New local patches:
     - Drop v8stdint.h header (above)
  * Cherry-picked upstream bugfix patches:
     - Fix SerialImpl constructor resource leak.
     - Handle 500kpbs serial ports.
     - Plug memory leak in exceptions.
     - Use MONOTONIC clock.

 -- Alec Leamas <leamas.alec@gmail.com>  Sat, 19 Sep 2020 12:13:27 +0200

libcxx-serial (1.2.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Drop unused build dependency (Closes: #943082)

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 06 Nov 2019 21:21:43 +0100


libcxx-serial (1.2.1-1) unstable; urgency=medium

  * Initial release (Closes: #917531)

 -- Alec Leamas <leamas.alec@gmail.com>  Thu, 27 Dec 2018 14:57:49 -0500
